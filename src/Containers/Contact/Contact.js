import React from 'react';
import "./Contact.css";

export default function Contact() {
    return (
        <div className="container-contact">
            <h1>Contactez-nous</h1>
            <p className="item">Par mail : xxxxxx</p>
            <p className="item">Par Téléphone : xx-xx-xx</p>
            <p className="item">Réseaux sociaux</p>
            <ul>
                <li>Twitter</li>
                <li>Facebook</li>               
            </ul>
        </div>
    )
}
