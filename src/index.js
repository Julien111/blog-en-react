import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux';
import store from './redux/store';

//Provider il va hydrater toute notre application en props on lui passe le store

ReactDOM.render(  
  <Provider store={store}>
    <App /> 
  </Provider>,
  document.getElementById('root')
);

