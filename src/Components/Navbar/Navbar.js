import React, { useState, useEffect } from "react";
import "./Navbar.css";
import {Link} from "react-router-dom";

export default function Navbar() {
  const [toggleMenu, setToggleMenu] = useState(false);
  const [dimension, setDimension] = useState(window.innerWidth);

  const afficheMenu = () => {
    setToggleMenu(!toggleMenu);
  };

  useEffect(() => {
    function changeWidth() {
      setDimension(window.innerWidth);
    }

    window.addEventListener("resize", changeWidth);

    return () => window.removeEventListener("resize", changeWidth);
  }, []);

  return (
    <nav>
      {(dimension > 500 || toggleMenu) && (
        <ul className="liste">
          <Link to="/">
            <li className="items">Accueil</li>
          </Link>

          <Link to="/ecrire">
            <li className="items">Écrire</li>
          </Link>

          <Link to="/contact">
            <li className="items">Contact</li>
          </Link>
        </ul>
      )}
      <button onClick={afficheMenu} className="btn">
        Menu
      </button>
    </nav>
  );
}
