const INITIAL_STATE = {
    articles: [],
}
// c'est là où on recevoir tous les articles qu'on souhaite afficher de l'Api


// pour addArticle on crée un nouveau tableau avec le state et on récup les données du form qu'on envoit au début du array
// action.payload récup les datas du form

function articleReducer (state = INITIAL_STATE, action) {

    switch (action.type) {
      case "ADDARTICLE": 
      const newArr = [...state.articles];
      newArr.unshift(action.payload);
        return {
         articles: newArr,
        };
      
      case "LOADARTICLES": {
        return {
          ...state,
          articles: action.payload,
        };
      }
    }
    return state;
}

export default articleReducer;

// en fonction de action.type on va avoir le switch

// Ici on va créer un state qui va stocker le contenu de notre appel Api (article blog)

// en dessous notre appel à Api

export const getArticles = () => dispatch => {

    fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json()).then(data => {
        dispatch({
            type: 'LOADARTICLES',
            payload: data,
        })
    })

}