import {createStore, applyMiddleware, combineReducers} from "redux";
import articleReducer from './articles/articleReducer';
import thunk from "redux-thunk";
// On importe redux plus thunk et le reducer dont on va avoir besoin


// combine reducer si on veut utiliser plusieurs reducers
const rootReducer = combineReducers({articleReducer});


// On crée le store on récup articleReducer et on le combine middleware pour un appel async
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;

//bien export pour l'utiliser